﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BasicClass;

namespace Client
{
    public class Client : BasicClass.BasicClass
    {
        /// <summary>
        /// Адрес диспетчера
        /// </summary>
        IPEndPoint dispatcher;

        /// <summary>
        /// Доступность диспетчера
        /// </summary>
        public bool Connected { get; private set; }

        Dictionary<ServerFunction, IPEndPoint> LastServer = new Dictionary<ServerFunction, IPEndPoint>();

        /// <summary>
        /// Создает клиента, который ищет диспетчера по заднному порту
        /// </summary>
        /// <param name="datagramport"></param>
        public Client(int datagramport)
            : base(datagramport)
        {
            port = 1500;
            whoiam = WhoIAm.Client;
            Connected = false;
            foreach (ServerFunction item in Enum.GetValues(typeof(ServerFunction)))
                LastServer.Add(item, new IPEndPoint(IPAddress.Any, 0));
        }

        /// <summary>
        /// Главная функция - преобразование строки
        /// </summary>
        /// <param name="input">Входная строка</param>
        /// <returns>Преобразованная строка</returns>
        public string Convert(string input, ServerFunction function)
        {
            string result = "";
            ChannelFactory<IServerFunc> scf;
            IPEndPoint ipend = ConnectServer(function);
            if (ipend.Port == 0)
                ipend = LastServer[function];
            scf = new ChannelFactory<IServerFunc>(new NetTcpBinding(SecurityMode.None), "net.tcp://" + ipend.Address.ToString() + ":" + ipend.Port.ToString());
            IServerFunc s = null;
            try
            {
                s = scf.CreateChannel();
                //Работает-вызываем функцию
                if (s.GetFunction() == function)
                    result = s.Convert(input);
                else
                    throw new Exception();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Сервер с выбранной функцией недоступен. Повторите попытку позднее", "Ошибка подключения");
            }
            finally
            {
                //Закрываем
                try { (s as ICommunicationObject).Close(); }
                catch { }
                scf.Close();
            }
            return result;
        }

        /// <summary>
        /// Подключение к системе. Поиск диспетчера
        /// </summary>
        public void Connect()
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //подключение сокета для ответа диспетчера
            while (true)
            {
                try
                {
                    //Создает подключение
                    IPEndPoint localPoint = new IPEndPoint(MyIP, port);
                    socket.Bind(localPoint);
                    socket.Listen(10);
                    //Console.WriteLine("Client is started, port = {0}", port);
                    break;
                }
                catch
                {
                    //Console.WriteLine("Port {0} is not available, try next port", port);
                    port = rand.Next(1000, 2000);
                }
            }
            //поиск диспетчера
            Thread t_ping = new Thread(new ThreadStart(PingDitpatcher));
            t_ping.Start();
            //подключение к нему
            dispatcher = ConnectDispatcher(socket);
            t_ping.Abort();
            socket.Close();
            try
            {
                //не нашли диспетчера
                if (dispatcher.Port == 0)
                    throw new Exception();
            }
            catch
            {
                return;
            }
            Connected = true;
            //теперь мы можешь обращаться к диспетчеру
        }

        /// <summary>
        /// Работа с диспетчером. (Он уже ответил)
        /// </summary>
        IPEndPoint ConnectDispatcher(Socket socket)
        {
            IPEndPoint ipend = new IPEndPoint(IPAddress.Any, 0);
            IPAddress ip;
            try
            {
                //Socket hanler = socket.Accept();
                #region Accept with timeout
                ManualResetEvent waitHandle = new ManualResetEvent(false);
                Socket hanler = null;

                socket.BeginAccept(delegate(IAsyncResult result)
                {
                    try
                    {
                        Socket listeningSocket = result.AsyncState as Socket;
                        hanler = listeningSocket.EndAccept(result);
                    }
                    catch (ObjectDisposedException)
                    {
                        // The socket timed out and the caller has already closed
                        // and disposed of the socket.
                    }
                    catch (Exception ex)
                    {
                        //System.Windows.Forms.MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        // Signal that we are done.
                        waitHandle.Set();
                    }

                }, socket);

                if (waitHandle.WaitOne(TimeSpan.FromMilliseconds(1000), false))
                {
                    if (hanler == null)
                        throw new Exception();
                }
                else
                { throw new Exception(); }
                #endregion
                ping = true;//Подключился
                //Console.WriteLine("Dispatcher's IP: {0}", hanler.RemoteEndPoint.ToString().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[0]);
                ip = IPAddress.Parse(hanler.RemoteEndPoint.ToString().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[0]);
                //получаем порт для подключения ремотинга
                byte[] data = new byte[Encoding.UTF8.GetBytes("0000").Count()];
                hanler.ReceiveTimeout = 5000;
                hanler.Receive(data);
                int d_port = Int32.Parse(Encoding.UTF8.GetString(data));
                ipend = new IPEndPoint(ip, d_port);
                hanler.Close();
            }
            catch(Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            return ipend;
        }

        /// <summary>
        /// Получения от диспетчера конечной точки сервера
        /// </summary>
        IPEndPoint ConnectServer(ServerFunction function)
        {
            IPEndPoint ipend = new IPEndPoint(IPAddress.Any, 0);
            //по полученному порту адресу получаем сервер дял работы
            try
            {
                IDispatcherFunc d = (IDispatcherFunc)Activator.GetObject(typeof(IDispatcherFunc), "tcp://" + dispatcher.Address.ToString() + ":" + dispatcher.Port.ToString() + "/Dispatcher.bin");
                ipend = d.GetServer(function);
                LastServer[function] = ipend;
            }
            catch
            {
                //Диспетчер не отвечает
                Connected = false;
            }
            return ipend;
        }
    }
}
