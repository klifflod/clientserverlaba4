﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BasicClass;

namespace Client
{
    public partial class ClientForm : Form
    {
        /// <summary>
        /// Объект-клиент
        /// </summary>
        Client client;

        /// <summary>
        /// Поток "дозвона"
        /// </summary>
        Thread tryconnect;

        bool need_to_ask = true;

        public ClientForm()
        {
            InitializeComponent();
        }

        private void ClientForm_Load(object sender, EventArgs e)
        {
            //cbInput.DataSource = Enum.GetValues(typeof(ServerFunction));
            List<string> list = new List<string>();
            //генерация списка из перечисления
            foreach (var item in Enum.GetValues(typeof(ServerFunction)))
            {
                FieldInfo fi = item.GetType().GetField(item.ToString());
                DescriptionAttribute[] attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(
                        typeof(DescriptionAttribute), false);
                list.Add(attributes[0].Description);
            }
            cbInput.DataSource = list;
            cbInput.SelectedIndex = 0;
            client = new Client(8000);
            client.Connect();
            //подключились?
            QuestionConnect();
        }

        private void bConvert_Click(object sender, EventArgs e)
        {
            rtbOutput.Text = client.Convert(rtbInput.Text, (ServerFunction)Enum.GetValues(typeof(ServerFunction)).GetValue(cbInput.SelectedIndex));
            //всё в порядке?
            QuestionConnect();
        }

        /// <summary>
        /// Спросить пользователя - стоит ли продолжать попытки соединения с диспетчером
        /// Да - запустить цикл попыток, иначе выйти
        /// </summary>
        void QuestionConnect()
        {
            if (!client.Connected && need_to_ask)
            {
                if (MessageBox.Show("Не удалось подключится к системе. Повторять попытки подключения?", "Ошибка подключения", MessageBoxButtons.RetryCancel) ==
                    System.Windows.Forms.DialogResult.Retry)
                {
                    try { tryconnect.Abort(); }
                    catch { }
                    tryconnect = new Thread(new ThreadStart(TryConnect));
                    tryconnect.Start();
                    need_to_ask = false;
                }
                else 
                    this.Close();
                //bConvert.Enabled = false;
            }
        }

        /// <summary>
        /// Попытка подключения с интервалом в 2 секунды
        /// </summary>
        void TryConnect()
        {
            while (!client.Connected)
            {
                Thread.Sleep(2000);
                client.Connect();
            }
            if (bConvert.InvokeRequired)
                this.Invoke(new Action(delegate() { Activateform(); }));
            else
                Activateform();
            need_to_ask = true;
        }

        /// <summary>
        /// Оповещение об успешном подключении
        /// </summary>
        void Activateform()
        {
            //bConvert.Enabled = true;
            MessageBox.Show("Соединение установлено");
        }

        private void ClientForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try { tryconnect.Abort(); }
            catch { }
        }
    }
}
