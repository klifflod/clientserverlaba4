﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BasicClass;

namespace Server
{
    /// <summary>
    /// Класс сервера
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Server : BasicClass.BasicClass, IServerFunc
    {
        /// <summary>
        /// Главная функция сервера, которую он предоставляет клиентам
        /// Точнее её описание, за счет которого идёт выбор
        /// </summary>
        protected ServerFunction function;

        #region IServerFunc Members
        public ServerFunction GetFunction()
        {
            Console.WriteLine("Send my function: {0}", function.ToString());
            return function;
        }

        public void SetFunction(ServerFunction function)
        {
            Console.WriteLine("Get my function: {0}", function.ToString());
            this.function = function;
        }

        public string Convert(string input)
        {
            Console.WriteLine("Convert text with function {0}", function.ToString());
            string result = "";
            switch (function)
            {
                case ServerFunction.KOI8toOEM866:
                    result = Encoding.GetEncoding("cp866").GetString(Encoding.GetEncoding("koi8-r").GetBytes(input));
                    break;
                case ServerFunction.OEM866toKOI8:
                    result = Encoding.GetEncoding("koi8-r").GetString(Encoding.GetEncoding("cp866").GetBytes(input));
                    break;
                case ServerFunction.ToUpper:
                    result = input.ToUpper();
                    break;
            }
            return result;
        }

        public bool Ping()
        {
            Console.WriteLine("Pong");
            //ping = true;
            pingtime = 10000;
            return true;
        }
        #endregion

        /// <summary>
        /// Создает сервер, которые прослушивает данный порт
        /// </summary>
        /// <param name="port">Порт для работы с клиентами</param>
        /// <param name="datagramport">Порт поиска диспетчера</param>
        /// <param name="function">Функция сервера</param>
        public Server(int port, int datagramport, ServerFunction function)
            : base(datagramport)
        {
            Console.WriteLine("Server created, my function: {0}", function.ToString());
            this.port = port;
            whoiam = WhoIAm.Server;
            this.function = function;
        }

        public Server(int datagramport, ServerFunction function)
            : base(datagramport)
        {
            this.port = 6000;
            whoiam = WhoIAm.Server;
            this.function = function;
            Console.WriteLine("Server created, my function: {0}", function.ToString());
        }

        /// <summary>
        /// Запуск сервера
        /// </summary>
        public void Start()
        {
            //Попытка подключения на этом порту
            while (true)
            {
                try
                {
                    //Создает подключение
                    IPEndPoint localPoint = new IPEndPoint(MyIP, port);
                    ServiceHost svh = new ServiceHost(this);
                    svh.AddServiceEndpoint(typeof(IServerFunc), new NetTcpBinding(SecurityMode.None), "net.tcp://" + MyIP.ToString() + ":" + port.ToString());
                    svh.Open();
                    Console.WriteLine("Server is started, port = {0}", port);
                    break;
                }
                catch
                {
                    //Генерируем другой порт
                    Console.WriteLine("Port {0} is not available, try next port", port);
                    port = rand.Next(6000, 7000);
                }
            }
            //Запускаем пингование диспетчера
            Thread t_ping = new Thread(new ThreadStart(PingDitpatcher));
            t_ping.Start();
            //Ждем команд
            while (true) ;
        }
    }
}
