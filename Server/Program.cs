﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BasicClass;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server(8000, ServerFunction.ToUpper);
            server.Start();
            Console.ReadKey();
        }
    }
}
