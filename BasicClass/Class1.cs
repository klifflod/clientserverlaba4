﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BasicClass 
{
    /// <summary>
    /// Перечисление для опознавания клиента от сервера
    /// </summary>
    public enum WhoIAm
    {
        Server, Client, Dispat
    }

    /// <summary>
    /// Список функций сервера
    /// </summary>
    public enum ServerFunction
    {
        [DescriptionAttribute("KOI8 в OEM866")]
        KOI8toOEM866,
        [DescriptionAttribute("OEM866 в KOI8")]
        OEM866toKOI8,
        [DescriptionAttribute("В верхний регистр")]
        ToUpper
    }

    /// <summary>
    /// Базовый класс, который прдоставляет основные поля и методы для работы любого компонента
    /// </summary>
    public abstract class BasicClass : MarshalByRefObject
    {
        /// <summary>
        /// Порт широковещательных запросов
        /// </summary>
        protected int datagramport;

        /// <summary>
        /// Мой хост
        /// </summary>
        protected IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

        /// <summary>
        /// Мой ip адрес
        /// </summary>
        protected IPAddress MyIP;

        /// <summary>
        /// Генератор случайных портов
        /// </summary>
        protected Random rand = new Random();

        /// <summary>
        /// Определение кто есть я
        /// </summary>
        protected WhoIAm whoiam;

        /// <summary>
        /// Переменная, которая должна говорит, ответил ли диспетчер
        /// </summary>
        protected bool ping = false;

        /// <summary>
        /// Определяет время между пингами
        /// </summary>
        protected int pingtime = 1000;

        /// <summary>
        /// Основной порт для работы
        /// </summary>
        protected int port;

        /// <summary>
        /// Создает точку и указывает порт широковещательных запросов
        /// </summary>
        /// <param name="datagramport">порт широковещательного запроса</param>
        public BasicClass(int datagramport)
        {
            this.datagramport = datagramport;

            //Получаем тот адрес, который соответствует ipv4
            MyIP = host.AddressList.Single(o => o.AddressFamily == AddressFamily.InterNetwork);
            Console.WriteLine("My IP: {0}", MyIP.ToString());
        }

        public BasicClass()
        {
            this.datagramport = 8000;

            //Получаем тот адрес, который соответствует ipv4
            MyIP = host.AddressList.Single(o => o.AddressFamily == AddressFamily.InterNetwork);
            Console.WriteLine("My IP: {0}", MyIP.ToString());
        }

        /// <summary>
        /// Пока не получилось подключится, посылает широковещательный запрос
        /// </summary>
        protected void PingDitpatcher()
        {
            while (!ping)
            {
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                IPAddress addr = IPAddress.Parse("255.255.255.255");
                //socket.SendTo(BitConverter.GetBytes(sender_data.ToString().Count()), new IPEndPoint(addr, datagramport));
                socket.SendTo(Encoding.UTF8.GetBytes(whoiam.ToString() + ":" + port.ToString()), new IPEndPoint(addr, datagramport));
                //Console.WriteLine("Ping dispatcher with massage {0}", whoiam.ToString() + ":" + port.ToString());

                Thread.Sleep(pingtime);
            }
        }
    }

    /// <summary>
    /// Предоставляет интерфейс управления сервером и ссылку на его главную функцию
    /// </summary>
    [ServiceContract]
    public interface IServerFunc
    {
        /// <summary>
        /// Получить функцию ,которую предоставляет сервер
        /// </summary>
        /// <returns>Предоставляемая функция</returns>
        [OperationContract]
        ServerFunction GetFunction();

        /// <summary>
        /// Задать функцию, предоставляемую сервером
        /// </summary>
        /// <param name="function">Предоставляемая функция</param>
        [OperationContract]
        void SetFunction(ServerFunction function);

        /// <summary>
        /// Преобразует строку по функции сервера
        /// </summary>
        /// <param name="input">Входная строка</param>
        /// <returns>Преобразованная строка</returns>
        [OperationContract]
        string Convert(string input);

        /// <summary>
        /// Проверяет состояние сервера
        /// </summary>
        [OperationContract]
        bool Ping();
    }

    /// <summary>
    /// Представляет собой интерфейс для функций диспетчера
    /// </summary>
    public interface IDispatcherFunc
    {
        IPEndPoint GetServer(ServerFunction function);
    }
}
