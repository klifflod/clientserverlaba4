﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BasicClass;

namespace Dispatcher
{
    /// <summary>
    /// Класс диспетчера
    /// </summary>
    public class Dispatcher : BasicClass.BasicClass, IDispatcherFunc
    {
        /// <summary>
        /// Сокет для получения широковещательных запросов
        /// </summary>
        Socket datagramsocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        /// <summary>
        /// Список адресов серверов
        /// </summary>
        Dictionary<IPEndPoint, ServerFunction> servers = new Dictionary<IPEndPoint, ServerFunction>();

        /// <summary>
        /// Собранная статистика работы для функций
        /// </summary>
        Dictionary<ServerFunction, int> statistic = new Dictionary<ServerFunction, int>();

        /// <summary>
        /// Объект блокировки списка серверов
        /// </summary>
        private Object thisLock = new Object();

        #region DispatcherFunc
        public IPEndPoint GetServer(ServerFunction function)
        {
            //запоминаем статистику
            statistic[function]++;
            
            lock (thisLock)
                try
                {
                    foreach (var item in servers)
                    {
                        if (item.Value == function)
                        {
                            ChannelFactory<IServerFunc> scf = null;
                            IServerFunc s = null;
                            try
                            {
                                scf = new ChannelFactory<IServerFunc>(new NetTcpBinding(SecurityMode.None), "net.tcp://" + item.Key.Address.ToString() + ":" + item.Key.Port.ToString());
                                s = scf.CreateChannel();
                                //доступен ли сервер?
                                if (s.Ping())
                                    return item.Key;
                            }
                            //иначе удалить его
                            catch { go_remove.Set(); }
                            finally
                            {
                                try { (s as ICommunicationObject).Close(); }
                                catch { }
                                scf.Close();
                            }
                        }
                    }
                }
                catch { }
                //в итоге нужно проверить на необходимость изменения списка серверов
                finally { go_rebalansed.Set(); }
            //пустой-сервер не найден
            return new IPEndPoint(IPAddress.Any, 0);
        }
        #endregion

        /// <summary>
        /// Создает экземпляр диспетчера
        /// </summary>
        /// <param name="datagramport">Порт, по которому диспетчер слушает широковещательные запросы</param>
        public Dispatcher(int datagramport)
            : base(datagramport)
        {
            port = 3000;
            whoiam = WhoIAm.Dispat;
            Console.WriteLine("Dispatcher created at port: {0}", datagramport);
            foreach (ServerFunction item in Enum.GetValues(typeof(ServerFunction)))
                statistic.Add(item, 0);
        }

        public Dispatcher()
            : base()
        {
            port = 3000;
            whoiam = WhoIAm.Dispat;
            Console.WriteLine("Dispatcher created at port: {0}", datagramport);
            foreach (ServerFunction item in Enum.GetValues(typeof(ServerFunction)))
                statistic.Add(item, 0);
        }

        /// <summary>
        /// Запуск диспетчера
        /// </summary>
        public void Start()
        {
            IPEndPoint localPoint = new IPEndPoint(MyIP, datagramport);
            //подписываемся на получение широковещательного
            try
            {
                //Подключаемся
                Console.WriteLine("Dispatcher is starting");
                datagramsocket.Bind(localPoint);
            }
            catch
            {
                Console.WriteLine("Another dispatcher is working, can't start second");
                return;
            }
            //проверяем в течении 3 секунд, нет ли еще кого-то, кто шлет широковещалку как диспетчер
            bool have_another_disp = false;
            DateTime told = DateTime.Now;
            DateTime tnew = DateTime.Now;
            datagramsocket.ReceiveTimeout = 3000;
            while (tnew.Subtract(told).Seconds < 3 && !have_another_disp)
            {
                byte[] data = new byte[Encoding.UTF8.GetBytes(WhoIAm.Server.ToString()).Count() + 5];
                EndPoint server_endpoint = new IPEndPoint(IPAddress.Any, 0);
                try
                {
                    tnew = DateTime.Now;
                    datagramsocket.ReceiveFrom(data, ref server_endpoint);
                    string[] s_data = Encoding.UTF8.GetString(data, 0, data.Count()).Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    //кто это? диспетчер?
                    if ((WhoIAm)Enum.Parse(typeof(WhoIAm), s_data[0]) == WhoIAm.Dispat)
                        have_another_disp = true;
                }
                catch { /*обрабатыывается ниже*/ }
            }
            if (have_another_disp)
            {
                Console.WriteLine("Another dispatcher is working, can't start second");
                return;
            }
            else
                Console.WriteLine("Dispatcher is started");
            datagramsocket.ReceiveTimeout = 0;
            //запускаем сингал, что мы диспетчер, единственный
            Thread t_ping = new Thread(new ThreadStart(PingDitpatcher));
            t_ping.Start();
            //регистрируем ремотинг
            while (true)
            {
                try
                {
                    ChannelServices.RegisterChannel(new TcpChannel(port), false);
                    //RemotingConfiguration.RegisterWellKnownServiceType(typeof(Dispatcher), "Dispatcher.bin", WellKnownObjectMode.Singleton);
                    RemotingServices.Marshal(this, "Dispatcher.bin");
                    Console.WriteLine("Dispetcher remoting function are registy at port {0}", port);
                    break;
                }
                catch
                {
                    Console.WriteLine("Port {0} is not available, try next port", port);
                    port = rand.Next(3000, 4000);
                }
            }
            //запускает поток перебалансировки
            Thread t_balance = new Thread(new ThreadStart(Rebalansed));
            t_balance.Start();
            //запускаем поток для удаления серверов
            Thread t_remove = new Thread(new ThreadStart(Removing));
            t_remove.Start();
            //ждем сигналов
            Wait();
        }

        /// <summary>
        /// Ждем сообщения\nЕсли от серверов, то сохраняем их адреса и функции
        /// \nИначе это клиенты - перенаправляем их куда они хотят
        /// </summary>
        void Wait()
        {
            while (true)
            {
                WhoIAm who;
                EndPoint server_endpoint = new IPEndPoint(IPAddress.Any, 0);
                byte[] data;
                string[] s_data = new string[2];
                int port;
                try
                {
                    //ждем получения сообщения
                    data = new byte[Encoding.UTF8.GetBytes(WhoIAm.Server.ToString()).Count() + 5];
                    datagramsocket.ReceiveFrom(data, ref server_endpoint);
                    s_data = Encoding.UTF8.GetString(data, 0, data.Count()).Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    //кто это?
                    who = (WhoIAm)Enum.Parse(typeof(WhoIAm), s_data[0]);
                    //откуда это?
                    port = Int32.Parse(s_data[1]);
                }
                catch
                {
                    Console.WriteLine("Can't get or descrypt message");
                    continue;
                }
                switch (who)
                {
                    //это сервер - добавляем его в список
                    case WhoIAm.Server:
                        Console.WriteLine("Server send message");
                        //посылаем ответ
                        AddServer(IPAddress.Parse(server_endpoint.ToString().Split(new char[] { ':' })[0]), port);
                        break;

                    //это клиент - посылаем ему свой порт для связи по ремотингу
                    case WhoIAm.Client:
                        Console.WriteLine("Client send message");
                        try
                        {
                            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            string[] param = server_endpoint.ToString().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                            Console.WriteLine("Client's IP: {0}, port: {1}", param[0], port);
                            //хотим послать наш порт для ремотинга
                            socket.Connect(IPAddress.Parse(param[0]), port);
                            //посылаем
                            socket.Send(Encoding.UTF8.GetBytes(this.port.ToString()));
                            socket.Close();
                            Console.WriteLine("Send to client my port = {0}", this.port);
                        }
                        catch
                        {
                            Console.WriteLine("Can't send to client");
                        }
                        break;
                    //Себя игнорируем
                    case WhoIAm.Dispat:
                        break;
                }
            }
        }

        /// <summary>
        /// Запрашиваем у сервера его функцию и добавляем это в список
        /// </summary>
        /// <param name="ip">Адрес сервера</param>
        private void AddServer(IPAddress ip, int port)
        {
            //Откываем канал
            ChannelFactory<IServerFunc> scf;
            scf = new ChannelFactory<IServerFunc>(new NetTcpBinding(SecurityMode.None), "net.tcp://" + ip.ToString() + ":" + port);
            IServerFunc s;
            s = scf.CreateChannel();
            try
            {
                //проверяем доступност ьсервера
                if (!s.Ping())
                    throw new Exception();
                //Работает-спрашиваем функцию
                ServerFunction server_func = s.GetFunction();
                IPEndPoint ipend = new IPEndPoint(ip, port);
                lock (thisLock)
                    if (!servers.ContainsKey(ipend))
                    {
                        //сервера нет, добавим его
                        servers.Add(ipend, server_func);
                        Console.WriteLine("Add new server with ip: {0}, port: {1} function: {2}", ip, port, server_func.ToString());
                    }
                    else
                    {
                        //сервер есть, проконтролируем чтобы функция была актуальной
                        Console.WriteLine("Change server with ip: {0}, port: {1}, new function: {2}", ip, port, server_func.ToString());
                        servers[ipend] = server_func;
                    }
            }
            catch
            {
                Console.WriteLine("Server with ip: {0}, port: {1} is not available", ip, port);
            }
            //Закрываем
            finally
            {
                try { (s as ICommunicationObject).Close(); }
                catch { }
                scf.Close();
            }
        }

        /// <summary>
        /// Флаг что пора начинать перебалансировку
        /// </summary>
        AutoResetEvent go_rebalansed = new AutoResetEvent(false);

        /// <summary>
        /// Перебалансировка
        /// </summary>
        void Rebalansed()
        {
            while (true)
            {
                //ждём
                go_rebalansed.WaitOne();

                Console.WriteLine("Start re-balance");
                //количество серверов
                int serv_count = servers.Count;
                Console.WriteLine("Server count: {0}", serv_count);
                if (serv_count > 0)
                {
                    //int func_count = Enum.GetValues(typeof(ServerFunction)).Length;
                    int sum = 0;
                    //необходимые значения
                    Dictionary<ServerFunction, int> new_count = new Dictionary<ServerFunction, int>();
                    //текущие значения
                    Dictionary<ServerFunction, int> current_count = new Dictionary<ServerFunction, int>();
                    //Множитель
                    int stat_sum = statistic.Sum(o => o.Value);
                    Console.Write("New count: ");
                    //вычисляем нужную пропорцию
                    foreach (ServerFunction item in Enum.GetValues(typeof(ServerFunction)))
                    {
                        current_count.Add(item, 0);
                        new_count.Add(item, 0);
                        if (item != ServerFunction.ToUpper)
                        {
                            int temp = (int)Math.Round((float)statistic[item] * (float)serv_count / (float)stat_sum);
                            new_count[item] = temp;
                            sum += temp;
                            Console.Write(" \"{0}\" = {1},", item, temp);
                        }
                    }
                    //остаток
                    Console.Write(" \"{0}\" = {1},", ServerFunction.ToUpper, serv_count - sum);
                    new_count[ServerFunction.ToUpper] = serv_count - sum;
                    Console.WriteLine();

                    //теперь сама перебалансировка
                    //список тех, кого надо изменить
                    List<IPEndPoint> needtorebalanse = new List<IPEndPoint>();
                    lock (thisLock)
                        //вычисляем текущую пропорцию
                        foreach (var serv in servers)
                            if (current_count[serv.Value] < new_count[serv.Value])
                                current_count[serv.Value]++;
                            else
                                needtorebalanse.Add(serv.Key);
                    //список тех, каких функций не хватает
                    List<ServerFunction> needtoadd = new List<ServerFunction>();
                    foreach (var item in current_count)
                        if (current_count[item.Key] < new_count[item.Key])
                            needtoadd.Add(item.Key);
                    //изменение функций
                    for (int i = 0; i < needtorebalanse.Count; i++)
                    {
                        ChannelFactory<IServerFunc> scf = null; ;
                        IServerFunc s = null;
                        try
                        {
                            scf = new ChannelFactory<IServerFunc>(new NetTcpBinding(SecurityMode.None), "net.tcp://" + needtorebalanse[i].Address.ToString() + ":" + needtorebalanse[i].Port);
                            s = scf.CreateChannel();
                            s.SetFunction(needtoadd[i]);
                            servers[needtorebalanse[i]] = needtoadd[i];
                        }
                        //ошибка, удалим сервер
                        catch { go_remove.Set(); }
                        finally
                        {
                            try { (s as ICommunicationObject).Close(); }
                            catch { }
                            scf.Close();
                        }
                    }
                }
                Console.WriteLine("End re-balance.");
            }
        }

        /// <summary>
        /// Флаг что пора начинать чистку серверов
        /// </summary>
        AutoResetEvent go_remove = new AutoResetEvent(false);

        /// <summary>
        /// Чистит список серверов
        /// </summary>
        void Removing()
        {
            while (true)
            {
                //ждём
                go_remove.WaitOne();
                Console.WriteLine("Removing start");
                //список на удаление
                List<IPEndPoint> remotelist = new List<IPEndPoint>();
                foreach (var item in servers)
                {
                    ChannelFactory<IServerFunc> scf = null;
                    IServerFunc s = null;
                    try
                    {
                        scf = new ChannelFactory<IServerFunc>(new NetTcpBinding(SecurityMode.None), "net.tcp://" + item.Key.Address.ToString() + ":" + item.Key.Port.ToString());
                        s = scf.CreateChannel();
                        //доступен?
                        if (!s.Ping())
                            throw new Exception();
                    }
                    catch
                    {
                        //недоступен:(
                        remotelist.Add(item.Key);
                    }
                    finally
                    {
                        try { (s as ICommunicationObject).Close(); }
                        catch { }
                        scf.Close();
                    }
                }
                lock (thisLock)
                    //удаляем неответивших
                    for (int i = remotelist.Count - 1; i >= 0; i--)
                    {
                        Console.WriteLine("Server with ip: {0}, port: {1} is removed", remotelist[i].Address, remotelist[i].Port);
                        servers.Remove(remotelist[i]);
                    }
                Console.WriteLine("Removing end");
            }
        }
    }
}
